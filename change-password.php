<?php

require_once dirname(__FILE__) . '/config.php';

if (isset($_POST['submit'])) {
    $oldPassword = mysqli_real_escape_string($dbc, trim($_POST['oldPassword']));
    $newPassword = mysqli_real_escape_string($dbc, trim($_POST['newPassword']));

    $query = "SELECT user_id FROM `signup` WHERE user_id = " . $_SESSION['user_id'] . "  AND password = SHA('$oldPassword')";
    $data = mysqli_query($dbc, $query);

    if (mysqli_num_rows($data) > 0) {
        $query = "UPDATE `signup` SET password = SHA('{$newPassword}') WHERE user_id = " . $_SESSION['user_id'];
        $data = mysqli_query($dbc, $query);
        $msg = "пароль успешно изменен";
    } else {
        $error = 'не правильно указан пароль';
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="style/style.css" rel="stylesheet">
</head>
<body>
<header>
    <ul>
        <li><a href="/">Главная</a></li>
        <li><a href="/">Новости</a></li>
        <li><a href="/">Музыка</a></li>
        <li><a href="/">Обратная связь</a></li>
    </ul>
</header>
<content>
    <?php if (isset($msg)): ?>
        <strong style="color: green;"><?= $msg ?> </strong>
    <?php endif; ?>

    <?php if (isset($error)): ?>
        <strong style="color: red;"><?= $error ?> </strong>
    <?php endif; ?>

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="username">Введите старый пароль:</label>
        <input type="text" name="oldPassword">

        <label for="password">Новый пароль:</label>
        <input type="password" name="newPassword">

        <button type="submit" name="submit">Вход</button>
    </form>
</content>
<footer class="clear">
    <p>Все права защищены</p>
</footer>

</body>

</html>